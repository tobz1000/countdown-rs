# Countdown doer

Been watching a lot of Cats Does Countdown lately and I've discovered I'm not good at playing along.
So I wrote something to do it for me.

The maths rounds should be optimal; the letters round is not perfect due to the dictionary used not
being the same.

## Usage

For numbers round:

`./countdown-rs [inputs] [target]`

e.g.:

`./countdown-rs 75 25 50 100 3 10 676`

Outputs:

```
Inputs: [75, 25, 50, 100, 3, 10]; target: 676
Solution: ((10 - (50 / 25)) * (75 - 3)) + 100 = 676
```

For letters round:

`./countdown-rs [8 or 9-letter string]`

e.g.:

`./countdown-rs aeiobcdfg`

Outputs:

```
Inputs: a e i o b c d f g
Longest: bodice (6 letters)
```
