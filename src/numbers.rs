use std::fmt::Display;

use itertools::Itertools;

const INPUT_COUNT: u8 = 6;

#[derive(Debug, Clone, Copy)]
pub struct Puzzle {
    pub in_nums: [i16; INPUT_COUNT as usize],
    pub target: i16,
}

fn result_distance(result: i16, target: i16) -> Option<i16> {
    result.checked_sub(target).map(|d| d.abs())
}

impl Puzzle {
    pub fn solve(&self) -> Solution {
        let mut curr_best: Option<Solution> = None;

        for leaf_count in 1..=INPUT_COUNT {
            let mut tree = Node::new(leaf_count, 0);

            loop {
                for ordered_inputs in self.in_nums.iter().map(|&n| n).permutations(6) {
                    if let Some(result) = tree.eval(&ordered_inputs) {
                        let distance = result_distance(result, self.target);

                        let replace_best = match (&curr_best, distance) {
                            (_, None) => false,
                            (None, Some(_)) => true,
                            (Some(curr_best), Some(distance)) => {
                                let curr_best_distance =
                                    result_distance(curr_best.result, self.target).unwrap();
                                distance < curr_best_distance
                            }
                        };

                        if replace_best {
                            let new_best = Solution {
                                tree: tree.clone(),
                                ordered_inputs,
                                result,
                                distance: result_distance(result, self.target).unwrap(),
                            };

                            if distance == Some(0) {
                                return new_best;
                            }

                            curr_best = Some(new_best);
                        }
                    }
                }

                if let Some(next_tree) = tree.next() {
                    tree = next_tree;
                } else {
                    break;
                }
            }
        }

        return curr_best.unwrap();
    }
}

impl Display for Puzzle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Inputs: {:?}; target: {}", self.in_nums, self.target)
    }
}

#[derive(Debug)]
pub struct Solution {
    tree: Node,
    ordered_inputs: Vec<i16>,
    result: i16,
    distance: i16,
}

impl Display for Solution {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.distance > 0 {
            write!(f, "Closest solution ({} away): ", self.distance)?;
        } else {
            write!(f, "Solution: ")?;
        }

        self.tree.print(f, &self.ordered_inputs)?;
        write!(f, " = {}", self.result)?;

        Ok(())
    }
}

#[derive(Debug, Clone)]
struct Node {
    leaf_count: u8,
    first_leaf_ix: usize,
    variant: NodeVariant,
}

#[derive(Debug, Clone)]
enum NodeVariant {
    Branch {
        op: Operation,
        left: Box<Node>,
        right: Box<Node>,
    },
    Leaf,
}

impl Node {
    fn new(leaf_count: u8, first_leaf_ix: usize) -> Self {
        assert!(leaf_count > 0);

        let variant = if leaf_count == 1 {
            NodeVariant::Leaf
        } else {
            let left_leaf_count = leaf_count - 1;
            let left = Node::new(left_leaf_count, first_leaf_ix);
            let right = Node::new(1, first_leaf_ix + left_leaf_count as usize);

            NodeVariant::Branch {
                left: Box::new(left),
                right: Box::new(right),
                op: Operation::Add,
            }
        };

        Node {
            leaf_count,
            first_leaf_ix,
            variant,
        }
    }

    fn next(self) -> Option<Self> {
        let Node {
            leaf_count,
            first_leaf_ix,
            variant,
        } = self;

        match variant {
            NodeVariant::Branch { left, right, op } => {
                let left_leaf_count = left.leaf_count;
                let left_leaf_ix = left.first_leaf_ix;
                let right_leaf_count = right.leaf_count;
                let right_leaf_ix = right.first_leaf_ix;

                if let Some(next_op) = op.next() {
                    return Some(Node {
                        leaf_count,
                        first_leaf_ix,
                        variant: NodeVariant::Branch {
                            left,
                            right,
                            op: next_op,
                        },
                    });
                }

                if let Some(next_right) = right.next() {
                    return Some(Node {
                        leaf_count,
                        first_leaf_ix,
                        variant: NodeVariant::Branch {
                            left,
                            right: Box::new(next_right),
                            op: Operation::Add,
                        },
                    });
                }

                if let Some(next_left) = left.next() {
                    let next_right = Node::new(right_leaf_count, right_leaf_ix);

                    return Some(Node {
                        leaf_count,
                        first_leaf_ix,
                        variant: NodeVariant::Branch {
                            left: Box::new(next_left),
                            right: Box::new(next_right),
                            op: Operation::Add,
                        },
                    });
                }

                // When all left & right nodes have been exhausted, we shift one leaf node to the
                // right hand side, until the left-hand side has only one leaf node
                if left_leaf_count > 1 {
                    let next_left = Node::new(left_leaf_count - 1, left_leaf_ix);
                    let next_right = Node::new(right_leaf_count + 1, right_leaf_ix - 1);

                    return Some(Node {
                        leaf_count,
                        first_leaf_ix,
                        variant: NodeVariant::Branch {
                            left: Box::new(next_left),
                            right: Box::new(next_right),
                            op: Operation::Add,
                        },
                    });
                }

                None
            }
            NodeVariant::Leaf => None,
        }
    }

    fn eval(&self, ordered_inputs: &[i16]) -> Option<i16> {
        match &self.variant {
            NodeVariant::Branch {
                left, right, op, ..
            } => {
                let left_val = left.eval(ordered_inputs)?;
                let right_val = right.eval(ordered_inputs)?;

                match op {
                    Operation::Add => left_val.checked_add(right_val),
                    Operation::Sub => {
                        if left_val > right_val {
                            left_val.checked_sub(right_val)
                        } else {
                            None
                        }
                    }
                    Operation::Mul => left_val.checked_mul(right_val),
                    Operation::Div => {
                        if left_val.abs().checked_rem(right_val.abs()) == Some(0) {
                            left_val.checked_div(right_val)
                        } else {
                            None
                        }
                    }
                }
            }
            NodeVariant::Leaf => Some(ordered_inputs[self.first_leaf_ix]),
        }
    }

    fn print(&self, f: &mut std::fmt::Formatter<'_>, ordered_inputs: &[i16]) -> std::fmt::Result {
        match &self.variant {
            NodeVariant::Branch { left, right, op } => {
                let left_requires_brackets = match left.variant {
                    NodeVariant::Branch { op: left_op, .. } => !ops_are_associative(left_op, *op),
                    NodeVariant::Leaf => false,
                };

                let right_requires_brackets = match right.variant {
                    NodeVariant::Branch { op: right_op, .. } => !ops_are_associative(*op, right_op),
                    NodeVariant::Leaf => false,
                };

                if left_requires_brackets {
                    write!(f, "(")?;
                }
                left.print(f, ordered_inputs)?;
                if left_requires_brackets {
                    write!(f, ")")?;
                }

                write!(f, " {} ", op)?;

                if right_requires_brackets {
                    write!(f, "(")?;
                }
                right.print(f, ordered_inputs)?;
                if right_requires_brackets {
                    write!(f, ")")?;
                }
            }
            NodeVariant::Leaf => {
                write!(f, "{}", ordered_inputs[self.first_leaf_ix])?;
            }
        }

        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
enum Operation {
    Add,
    Sub,
    Mul,
    Div,
}

impl Display for Operation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Operation::Add => "+",
                Operation::Sub => "-",
                Operation::Mul => "*",
                Operation::Div => "/",
            }
        )
    }
}

impl Operation {
    fn next(&self) -> Option<Self> {
        use Operation::*;
        match self {
            Add => Some(Sub),
            Sub => Some(Mul),
            Mul => Some(Div),
            Div => None,
        }
    }
}

fn ops_are_associative(left: Operation, right: Operation) -> bool {
    use Operation::*;
    match (left, right) {
        (Add, Add | Sub) => true,
        (Mul, Mul | Div) => true,
        _ => false,
    }
}
