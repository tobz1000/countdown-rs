use std::{collections::HashSet, fmt::Display};

use itertools::Itertools;

/*
Sourced from: http://www.gwicks.net/textlists/ukenglish.zip
wordlist TODOs:
- would be nice to download this via build script instead of keeping in source code directly (with
appropriate caching)
  seems to have non-utf garbase in the last few lines which need removing (??)
- this list has proper nouns (and abbreviations?) in lowercase, which is disallowed. potential solutions:
  - cross-check with oxford dictionary api (not viable at build-time, would require internet connection
    at runtime, limited free plan) https://developer.oxforddictionaries.com/signup
  - cross check with: https://github.com/dwyl/english-words this isn't great as a primary source (seems
    to have too many false positives) but it does use capital letters, so could be used to verify words
    from the other source.
- somehow source the actual oxford dictionary
*/

const WORDS: &'static str = include_str!("words.txt");

pub struct Puzzle {
    pub letters: Vec<char>,
}

pub struct Solution {
    word: String,
}

impl Puzzle {
    pub fn solve(&self) -> Solution {
        let words: HashSet<_> = WORDS.split('\n').collect();

        for count in (1..=self.letters.len()).rev() {
            // For 9 letters or below, this brute-force search takes <1s, so good enough for our
            // purposes
            for perm in self.letters.iter().permutations(count) {
                let word: String = perm.into_iter().collect();
                if words.contains(word.as_str()) {
                    return Solution { word };
                }
            }
        }
        Solution {
            word: "".to_owned(),
        }
    }
}

impl Display for Puzzle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Inputs: {}", self.letters.iter().join(" "))
    }
}

impl Display for Solution {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Longest: {} ({} letters)", self.word, self.word.len())
    }
}
