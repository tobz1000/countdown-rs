mod letters;
mod numbers;

enum Input {
    NoInput,
    Invalid,
    Numbers(numbers::Puzzle),
    Letters(letters::Puzzle),
}

const USAGE: &'static str = "Usage:

Numbers round:
  ./countdown-rs [inputs] [target]
e.g.:
  ./countdown-rs 75 25 50 100 3 10 676
Outputs:
  Inputs: [75, 25, 50, 100, 3, 10]; target: 676
  Solution: ((10 - (50 / 25)) * (75 - 3)) + 100 = 676

Letters round:
  ./countdown-rs [8 or 9-letter string]
e.g.:
  ./countdown-rs aeiobcde
Outputs:
  [TBD]
";

fn parse_args() -> Input {
    let args: Vec<_> = std::env::args().skip(1).collect();

    match args.len() {
        0 => Input::NoInput,
        1 => {
            let letters_str = &args[0];

            if !letters_str.len() == 8 && !letters_str.len() == 9 {
                Input::Invalid
            } else if letters_str.chars().any(|c| !c.is_ascii_alphabetic()) {
                Input::Invalid
            } else {
                let letters = letters_str
                    .chars()
                    .map(|c| c.to_ascii_lowercase())
                    .collect();
                Input::Letters(letters::Puzzle { letters })
            }
        }
        7 => {
            let parsed: Vec<i16> = match args
                .into_iter()
                .map(|a| a.parse())
                .collect::<Result<Vec<i16>, _>>()
            {
                Ok(p) => p,
                Err(_) => return Input::Invalid,
            };

            Input::Numbers(numbers::Puzzle {
                in_nums: [
                    parsed[0], parsed[1], parsed[2], parsed[3], parsed[4], parsed[5],
                ],
                target: parsed[6],
            })
        }
        _ => Input::Invalid,
    }
}

fn main() {
    use Input::*;
    match parse_args() {
        Numbers(puzzle) => {
            let sol = puzzle.solve();

            println!("{puzzle}");
            println!("{sol}");
        }
        Letters(puzzle) => {
            let sol = puzzle.solve();

            println!("{puzzle}");
            println!("{sol}");
        }
        Invalid => {
            println!("Invalid input");
            println!("{USAGE}");
        }
        NoInput => {
            println!("{USAGE}");
        }
    }
}
